// conf.js
exports.config = {
  sauceUser: 'paullittlebury',
  sauceKey: '853bb7c5-423c-4c3f-bee1-a4810b079215',

  //seleniumAddress: 'http://ondemand.saucelabs.com:80/wd/hub',
  framework: 'cucumber',
  specs: [
    'features/*.feature'
  ],

  // restartBrowserBetweenTests: true,

  onPrepare: function(){
    var caps = browser.getCapabilities()
  },

  multiCapabilities: [{
    browserName: 'firefox',
    version: '32',
    platform: 'OS X 10.10',
    name: "firefox-tests",
    shardTestFiles: true,
    maxInstances: 25
  }, {
    browserName: 'chrome',
    version: '41',
    platform: 'Windows 7',
    name: "chrome-tests",
    shardTestFiles: true,
    maxInstances: 25
  }],

  resultJsonOutputFile: 'report.json',

  cucumberOpts: {
    require: 'features/steps/*_steps.js',
    format: 'pretty'
  },

  onComplete: function() {

    var printSessionId = function(jobName){
      browser.getSession().then(function(session) {
        console.log('SauceOnDemandSessionID=' + session.getId() + ' job-name=' + jobName);
      });
    }
    printSessionId("Cucumber JS Test");
  }
}
