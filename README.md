# cucumberjs with protractor example

## Getting Started


### Install Dependencies

```
npm install
```

### Run Protractor

```
npm run protractor
```

### Run tests through saucelabs


```
npm run saucelabs
```

My node and npm versions (Ubuntu):

node:5.4.1
npm:3.3.12

